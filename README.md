# getmbrs
Select member galaxies given a cluster and a galaxy catalog.
Select member galaxies given a cluster and a galaxy FITS catalog.

Copyright 2016, 2017 Francesco Montanari, Ghassem Gozaliasl.

@@ -26,4 +26,4 @@ installation as:
python -m getmbrs.getmbrs ARGUMENTS
```
(Running `python getmbrs/getmbrs.py ...` will raise `ValueError:
Attempted relative import in non-package`.)
\ No newline at end of file
Attempted relative import in non-package`.)